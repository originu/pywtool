from modules.docker.service.docker_service import DockerService

__author__ = 'kevin'

from context import app
from flask import json, Response

service = DockerService()

@app.route("/docker/images", methods=[ 'GET' ])
def images():
    return Response( json.dumps( service.get_images() ), status = 200, mimetype = "application/json" )

@app.route("/docker/containers", methods=[ 'GET' ])
def containers():
    return Response( json.dumps( service.get_containers() ), status = 200, mimetype = "application/json" )