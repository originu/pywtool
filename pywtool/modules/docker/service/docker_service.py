__author__ = 'kevin'

import requests

class DockerService(object):
    DOCKER_SERVICE_URL = "http://192.168.0.11:4243"

    def get_images(self):
        r = requests.get( self.DOCKER_SERVICE_URL + "/images/json" )
        return r.json()

    def get_containers(self):
        r = requests.get( self.DOCKER_SERVICE_URL + "/containers/json" )
        return r.json()