from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

__author__ = 'kevin'

from flask import Flask

# flask
app = Flask(__name__, instance_path="/pywtool")

# sqlalchemy
engine = create_engine('mysql+mysqlconnector://peyto:000000@192.168.137.110:3306/peytodb', echo=True)
Base = declarative_base()
Base.metadata.create_all(engine)

Session = sessionmaker(bind=engine)
session = Session()

# session = db.

# main
import core.controller
import modules.docker.controller

# docker app