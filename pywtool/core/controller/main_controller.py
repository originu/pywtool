from json import dumps
from sqlalchemy.ext import serializer
from core.entity.UserEntity import UserEntity
from core.repository.UserRepository import UserRepository
from core.util.jsonutil import model_to_json

__author__ = 'kevin'

from datetime import datetime
from flask import render_template, jsonify

from context import app

@app.route("/")
@app.route("/home")
def home():
    """Renders the home page."""
    userRepo = UserRepository()
    # user1 = UserEntity( name = 'kevin1', fullname = 'kevinryu1', password = '0000' )
    # userRepo.add(user1)
    model = userRepo.get( 1 )
    return render_template( "pages/index.html", obj = model_to_json( model ) )

@app.route("/login")
def login():
    return render_template( "pages/login.html" )

@app.route("/grid")
def grid():
    return render_template( "pages/grid.html" )

@app.route("/tables")
def tables():
    return render_template( "pages/tables.html" )


