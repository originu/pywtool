from flask import json
from sqlalchemy import Column, String
from sqlalchemy import Integer
from context import Base

__author__ = 'kevin'

class UserEntity( Base ):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    fullname = Column(String)
    password = Column(String)

    def __init__(self):
        pass

    def __init__(self, name, fullname, password):
        self.name = name
        self.fullname = fullname
        self.password = password
        pass

    def __repr__(self):
        return "<User(name='%s', fullname='%s', password='%s')>" %\
               (self.name, self.fullname, self.password)