__author__ = 'kevin'

# converts sqlalchmey orm class to json
def model_to_json(model):
    json = {}
    for col in model._sa_class_manager.mapper.mapped_table.columns:
        json[col.name] = getattr(model, col.name)
    return json