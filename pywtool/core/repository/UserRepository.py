from context import session
from core.entity.UserEntity import UserEntity

__author__ = 'kevin'

class UserRepository():

    def add(self, user):
        session.add(user)
        session.commit()

    def get(self, id):
        return session.query(UserEntity).filter(UserEntity.id == id).one()
        # for row in session.query(UserEntity).filter(UserEntity.id == id):
        #     print( row )
        # return 1
